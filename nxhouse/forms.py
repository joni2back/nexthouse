# -*- coding:utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _
from django.contrib.auth.forms import UserRegistrationForm
from django.contrib.auth.models import User
from nxhouse.models import Item, Group, Profile
import logging
import re

class SearchForm(forms.Form):
    group = forms.ModelChoiceField(label=_('Group'), queryset=Group.objects.all(), required=False)
    q = forms.CharField(required=False, label=_('Query'),)
    price_from = forms.DecimalField(required=False, label=_('price_from'),)
    price_to = forms.DecimalField(required=False, label=_('price_to'),)

    def filter_by(self):
        # TODO search using more than one field
        # TODO split query string and make seaprate search by words
        logger = logging.getLogger(__name__)
        logger.error(self.cleaned_data)

        filters = {}
        filters['description__icontains'] = self.cleaned_data['q']

        if self.cleaned_data['group']:
            filters['group'] = self.cleaned_data['group']

        if self.cleaned_data['price_from'] is not None:
            filters['price__gte'] = self.cleaned_data['price_from']

        if self.cleaned_data['price_to'] is not None:
            filters['price__lte'] = self.cleaned_data['price_to']


        return filters


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ('group', 'title', 'description', 'price', 'is_active')


class ProfileForm(forms.ModelForm):
    first_name = forms.CharField(required=False, label=_('First name'))
    last_name = forms.CharField(required=False, label=_('Last name'))
    
    def __init__(self, *args, **kw):
        super(ProfileForm, self).__init__(*args, **kw)
        self.fields['first_name'].initial = self.instance.user.first_name
        self.fields['last_name'].initial = self.instance.user.last_name

    def save(self, *args, **kw):
        initial = super(ProfileForm, self).save(*args, **kw)
        self.instance.user.first_name = self.cleaned_data.get('first_name')
        self.instance.user.last_name = self.cleaned_data.get('last_name')
        self.instance.user.save()
        return initial

    class Meta:
        model = Profile
        fields = ('phone', 'first_name', 'last_name')


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    #email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    def clean_password1(self):
        password1 = self.cleaned_data['password1']
        if not re.match(r'[\w]{8,}', password1):
            raise forms.ValidationError("weak passwd")

        return password1

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'password1', 'password2', )

