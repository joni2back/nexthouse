# -*- coding:utf-8 -*-
from django.conf import settings

from nxhouse import settings as nxhouse_settings


def common_values(request):
    return {
        'NXHOUSE_CURRENCY': nxhouse_settings.NXHOUSE_CURRENCY,
        'NXHOUSE_DISPLAY_CREDITS': nxhouse_settings.NXHOUSE_DISPLAY_CREDITS,
        'NXHOUSE_LOGIN_TO_CONTACT': nxhouse_settings.NXHOUSE_LOGIN_TO_CONTACT,
        'NXHOUSE_SITE_DESCRIPTION': nxhouse_settings.NXHOUSE_SITE_DESCRIPTION,
        'NXHOUSE_SITE_NAME': nxhouse_settings.NXHOUSE_SITE_NAME,
        'FACEBOOK_APP_ID': getattr(settings, 'FACEBOOK_APP_ID', ''),
        'GOOGLE_ANALYTICS_PROPERTY_ID': getattr(settings, 'GOOGLE_ANALYTICS_PROPERTY_ID', ''),
        'GOOGLE_SITE_VERIFICATION_ID': getattr(settings, 'GOOGLE_SITE_VERIFICATION_ID', ''),
    }
