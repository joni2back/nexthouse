# -*- coding:utf-8 -*-
from django.contrib.sites.models import Site
from django.contrib.syndication.views import Feed

from nxhouse import settings as nxhouse_settings
from nxhouse.models import Item


class LatestItemFeed(Feed):
    # TODO Should return proper XML content type
    link = '/'
    description = nxhouse_settings.NXHOUSE_SITE_NAME

    def title(self):
        return Site.objects.get_current().name

    def items(self):
        return Item.objects.all().order_by('-updated')[:nxhouse_settings.NXHOUSE_RSS_LIMIT]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description[:200]
