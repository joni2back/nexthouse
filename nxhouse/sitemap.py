from django.contrib.sitemaps import Sitemap

from nxhouse import settings as nxhouse_settings
from nxhouse.models import Item


class ItemSitemap(Sitemap):
    def changefreq(self, obj):
        return 'weekly'

    def lastmod(self, obj):
        return obj.updated

    def items(self):
        return Item.objects.all()[:nxhouse_settings.NXHOUSE_SITEMAP_LIMIT]


sitemaps_dict = {
    'Item': ItemSitemap,
}
