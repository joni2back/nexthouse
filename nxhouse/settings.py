"""
Default settings for nxhouse app.
"""

from django.conf import settings

# Site currency
NXHOUSE_CURRENCY = getattr(settings, 'NXHOUSE_CURRENCY', 'USD')

# Display credits in footer
NXHOUSE_DISPLAY_CREDITS = getattr(settings, 'NXHOUSE_DISPLAY_CREDITS', True)

# Max items per page
NXHOUSE_ITEM_PER_PAGE = getattr(settings, 'NXHOUSE_ITEM_PER_PAGE', 10)

# Max items per user
NXHOUSE_ITEM_PER_USER_LIMIT = getattr(settings, 'NXHOUSE_ITEM_PER_USER_LIMIT', 20)

# Hide contact details for unauthorized users
NXHOUSE_LOGIN_TO_CONTACT = getattr(settings, 'NXHOUSE_LOGIN_TO_CONTACT', True)

# Max related items displayed on page
NXHOUSE_RELATED_LIMIT = getattr(settings, 'NXHOUSE_RELATED_LIMIT', 6)

# Max items included to the RSS feed
NXHOUSE_RSS_LIMIT = getattr(settings, 'NXHOUSE_RSS_LIMIT', 100)

# Site default description
NXHOUSE_SITE_DESCRIPTION = getattr(settings, 'NXHOUSE_SITE_DESCRIPTION', 'Classified Advertisements Powered by Django NXHOUSE')

# Default site name
NXHOUSE_SITE_NAME = getattr(settings, 'NXHOUSE_SITE_NAME', 'NXHOUSE')

# Sitemap items limit
NXHOUSE_SITEMAP_LIMIT = getattr(settings, 'NXHOUSE_SITEMAP_LIMIT', 1000)
