# -*- coding:utf-8 -*-

from django.apps import AppConfig


class NXHOUSEConfig(AppConfig):
    name = 'nxhouse'
    verbose_name = "NXHOUSE - Django Classified"
